package com.vikash.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vikash.dao.OperateurDao;
import com.vikash.modal.OperateurModel;
import com.vikash.model.bean.OperateurAdapter;
import com.vikash.model.bean.OperateurJsonObject;
import com.vikash.repository.OperateurRepository;
import com.vikash.utils.Toolkit;



@Controller
public class ApplicationController {

	
	@Autowired
	private OperateurDao operateurDao;
	
	@Autowired
	OperateurRepository operateurRepository;
	
	@RequestMapping("/dashboard")
	public String Welcome(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_HOME");
		return "index";
	}

	@RequestMapping(value= {"/exploitant"} , method=RequestMethod.GET)
	public String exploitant(ModelMap model, HttpServletRequest request) {
	//	OperateurModel om = new OperateurModel();
	  
	  model.addAttribute("base_url", Toolkit.getBaseUrl(request));
	  
	  List<OperateurModel> listeOperateur = operateurDao.findAll();
	  OperateurModel om = listeOperateur.get(0);
	  model.addAttribute("list", listeOperateur);
		model.addAttribute("om",om);	
		return "listeExploitant";
	}
	
	@RequestMapping(value={"/list_operateur"}, method=RequestMethod.GET, produces = "application/json")
	public @ResponseBody String  afficheListeOperateur(HttpServletRequest request) {
    
	  Integer pageNumber = 1;
    if (StringUtils.isNotBlank(request.getParameter("pageNumber"))) {
      pageNumber = Integer.valueOf(request.getParameter("pageNumber"));
      if (pageNumber == 0) pageNumber = 1;
    }

    Integer pageDisplayLength = 10;
    if (StringUtils.isNotBlank(request.getParameter("limit"))) {
      pageDisplayLength = Integer.valueOf(request.getParameter("limit"));
    }
	  
    HashMap<String, Object> parameterMap = new HashMap<>();
    
    parameterMap.put("offset", pageNumber);
    parameterMap.put("limit", pageDisplayLength);
    
    Map<String, Object> data = operateurDao.findAllByParam(parameterMap);
    
    @SuppressWarnings("unchecked")
    List<OperateurModel> listeCarte = (List<OperateurModel>) data.get("results");

    OperateurJsonObject OperateurJsonObject = new OperateurJsonObject();
    OperateurJsonObject.setiTotalRecords((int) data.get("total"));
    OperateurJsonObject.setAaData(listeCarte);

    Gson gson = new GsonBuilder().registerTypeAdapter(OperateurModel.class, new OperateurAdapter(request)).create();
    
	  return gson.toJson(OperateurJsonObject);
	}

	@RequestMapping("/collecteur")
	public String collecteur(ModelMap model) {
		
		List<OperateurModel> list_collect = operateurRepository.findAll();
		model.addAttribute("list_collect",list_collect);		
		return "listeCollecteur";
	}

	@RequestMapping("/action")
	public String action(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "listeaction";
	}

	@RequestMapping("/perception")
	public String perception(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "gestionperception";
	}
	
	@RequestMapping("/exportation")
	public String exportation(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "exportation";
	}
	
	@RequestMapping("/user")
	public String user(ModelMap model) {
		
		return "user";
	}

	@RequestMapping(value="/user",method=RequestMethod.POST)
	public String userCreate(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "listeExploitant";
	}

//	@RequestMapping(value="/user")
//	public String saveRegistration(@Valid OperateurModel om,
//			BindingResult result,
//			ModelMap model,
//			RedirectAttributes redirectAttributes) {
//		
//		if(result.hasErrors()) {
//			return "/user";
//		}
//		
//		operateurDao.save(om);
//		
//		return "redirect:/exploitant";
//	}
	
//	@PostMapping("/user")
//	public OperateurModel createOperateur(@Valid @RequestBody OperateurModel om) {
//		return operateurDao.save(om);
//	}
//

	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "deconnection";
	}

}
