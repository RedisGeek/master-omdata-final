package com.vikash.modal;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name="operateur")
@EntityListeners(AuditingEntityListener.class)

public class OperateurModel implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	private int CODEEXPL;
	
	@Column
	private int CODETYP;
	
	@Column
	private String NOMEXPLOITANT;
	
	@Column
	private String PRENOMEXPL;
	
	@Column
	private Date DATENAISS;
	
	@Column
	private String SEXE;
	
	@Column
	private int AGE;
	
	@Column
	private String ADRESSE;
	
	@Column
	private String NUMTEL;
	
	@Column
	private String NUMCIN;

	@Column
	private Date DATECIN;
	
	@Column
	private String LIEUCIN;
	
	@Column
	private String COMMUNE;
	
	@Column
	private String NIF;

	public int getCODEEXPL() {
		return CODEEXPL;
	}

	public void setCODEEXPL(int cODEEXPL) {
		CODEEXPL = cODEEXPL;
	}

	public int getCODETYP() {
		return CODETYP;
	}

	public void setCODETYP(int cODETYP) {
		CODETYP = cODETYP;
	}

	public String getNOMEXPLOITANT() {
		return NOMEXPLOITANT;
	}

	public void setNOMEXPLOITANT(String nOMEXPLOITANT) {
		NOMEXPLOITANT = nOMEXPLOITANT;
	}

	public String getPRENOMEXPL() {
		return PRENOMEXPL;
	}

	public void setPRENOMEXPL(String pRENOMEXPL) {
		PRENOMEXPL = pRENOMEXPL;
	}

	public Date getDATENAISS() {
		return DATENAISS;
	}

	public void setDATENAISS(Date dATENAISS) {
		DATENAISS = dATENAISS;
	}

	public String getSEXE() {
		return SEXE;
	}

	public void setSEXE(String sEXE) {
		SEXE = sEXE;
	}

	public int getAGE() {
		return AGE;
	}

	public void setAGE(int aGE) {
		AGE = aGE;
	}

	public String getADRESSE() {
		return ADRESSE;
	}

	public void setADRESSE(String aDRESSE) {
		ADRESSE = aDRESSE;
	}

	public String getNUMTEL() {
		return NUMTEL;
	}

	public void setNUMTEL(String nUMTEL) {
		NUMTEL = nUMTEL;
	}

	public String getNUMCIN() {
		return NUMCIN;
	}

	public void setNUMCIN(String nUMCIN) {
		NUMCIN = nUMCIN;
	}

	public Date getDATECIN() {
		return DATECIN;
	}

	public void setDATECIN(Date dATECIN) {
		DATECIN = dATECIN;
	}

	public String getLIEUCIN() {
		return LIEUCIN;
	}

	public void setLIEUCIN(String lIEUCIN) {
		LIEUCIN = lIEUCIN;
	}

	public String getCOMMUNE() {
		return COMMUNE;
	}

	public void setCOMMUNE(String cOMMUNE) {
		COMMUNE = cOMMUNE;
	}

	public String getNIF() {
		return NIF;
	}

	public void setNIF(String nIF) {
		NIF = nIF;
	}

	
}
