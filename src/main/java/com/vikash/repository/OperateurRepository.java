package com.vikash.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vikash.modal.OperateurModel;


//@RepositoryRestResource(collectionResourceRel="exploit" , path="exploit")
@Repository
public interface OperateurRepository extends JpaRepository<OperateurModel, Integer>{

//  List<OperateurModel> findOperateurByParm(int offset, int size);
  
}
