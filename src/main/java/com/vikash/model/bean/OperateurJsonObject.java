package com.vikash.model.bean;

import java.util.List;

import com.vikash.modal.OperateurModel;

public class OperateurJsonObject {

  int total;

  String sEcho;

  String sColumns;

  List<OperateurModel> rows;

  public int getiTotalRecords() {
    return total;
  }

  public void setiTotalRecords(int iTotalRecords) {
    this.total = iTotalRecords;
  }

  public String getsEcho() {
    return sEcho;
  }

  public void setsEcho(String sEcho) {
    this.sEcho = sEcho;
  }

  public String getsColumns() {
    return sColumns;
  }

  public void setsColumns(String sColumns) {
    this.sColumns = sColumns;
  }

  public List<OperateurModel> getAaData() {
    return rows;
  }

  public void setAaData(List<OperateurModel> aaData) {
    this.rows = aaData;
  }
  
}
