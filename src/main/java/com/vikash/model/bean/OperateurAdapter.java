package com.vikash.model.bean;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.util.HtmlUtils;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.vikash.modal.OperateurModel;
import com.vikash.utils.Toolkit;

public class OperateurAdapter  extends TypeAdapter<OperateurModel>{

  final Gson embedded = new Gson();
  HttpServletRequest request;
  
  public OperateurAdapter(HttpServletRequest request) {
    super();
    this.request = request;
  }
  
  @Override
  public OperateurModel read(JsonReader arg0) throws IOException {
    return null;
  }

  @Override
  public void write(JsonWriter out, OperateurModel value) throws IOException {

    out.beginObject();
    out.name("code");
    embedded.toJson(embedded.toJsonTree(HtmlUtils.htmlEscape(""+value.getCODEEXPL())),
        out);
    out.name("nom");
    embedded.toJson(embedded.toJsonTree(HtmlUtils.htmlEscape(value.getNOMEXPLOITANT())),
        out);
    out.name("prenom");
    embedded.toJson(embedded.toJsonTree(HtmlUtils.htmlEscape(value.getPRENOMEXPL())),
        out);
    out.name("adresse");
    embedded.toJson(embedded.toJsonTree(HtmlUtils.htmlEscape(value.getADRESSE())),
        out);
    out.name("phone");
    embedded.toJson(embedded.toJsonTree(HtmlUtils.htmlEscape(value.getNUMTEL())),
        out);
    out.name("cin");
    embedded.toJson(embedded.toJsonTree(HtmlUtils.htmlEscape(""+value.getNUMCIN())),
        out);
    
    out.name("action");
    String action = "<ul class='data_action_style'>";
    
    action += "<li><i class='glyphicon glyphicon-search' aria-hidden='true' onClick='document.location.href = \""
        + Toolkit.getBaseUrl(request) + "/edit-carte?id=" + value.getCODEEXPL() + "\"'></i></li>";

    action += "</ul>";
    
    embedded.toJson(embedded.toJsonTree(action), out);

    out.endObject();
    
  }

}
