package com.vikash.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vikash.modal.OperateurModel;
import com.vikash.repository.OperateurRepository;
import com.vikash.utils.Toolkit;


@Service
public class OperateurDao {
	
	@Autowired
	OperateurRepository operateurRepository;
	
	//Ajout
	public OperateurModel save(OperateurModel om) {
		return operateurRepository.save(om);
	}
	
	//recherche
	public List<OperateurModel> findAll(){
		return operateurRepository.findAll();
	}	
	
	//get by id
	public OperateurModel findOne(Integer id) {
		return operateurRepository.findOne(id);
	}
	
	//supprimer
	public void delete(OperateurModel om) {
		operateurRepository.delete(om);
	}
	
	public Map<String, Object> findAllByParam(Map<String, Object> parameterMap) {
	  
	  Map<String, Object> data = new HashMap<>();
	  
	  List<OperateurModel> listeOperateur = new ArrayList<>();
	  
    Integer pageNumber = (Integer) parameterMap.get("offset");
    Integer pageDisplayLength = (Integer) parameterMap.get("limit");
    
    List<OperateurModel> total = operateurRepository.findAll();
    data.put("total", total.size());
    
    Query query = (Query) operateurRepository.findAll();//findOperateurByParm(Toolkit.calculOffset(pageNumber, pageDisplayLength), pageDisplayLength);
    
    query.setFirstResult(Toolkit.calculOffset(pageNumber, pageDisplayLength)).setMaxResults(pageDisplayLength);
    
    @SuppressWarnings("unchecked")
    List<OperateurModel> liste = query.getResultList();
    
    if (liste != null && (liste.size()) > 0) listeOperateur = liste;
    data.put("results", listeOperateur);
    
    return data;
	}
	
}
