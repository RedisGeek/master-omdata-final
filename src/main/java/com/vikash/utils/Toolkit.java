package com.vikash.utils;

import javax.servlet.http.HttpServletRequest;

public class Toolkit {

  /**
   * Utilisée pour la pagination
   * 
   * @param page
   * @param limite
   * @return
   */
  public static int calculOffset(int page, int limite) {
    return ((limite * page) - limite);
  }
  
  /**
   * Retourne l'url de base
   * 
   * @param request : type HttpServletRequest
   * @return url : type String
   */
  public static String getBaseUrl(HttpServletRequest request) {
    if ((request.getServerPort() == 80) || (request.getServerPort() == 443))
      return request.getScheme() + "://" + request.getServerName() + request.getContextPath();
    else return request.getScheme() + "://" + request.getServerName() + ":"
        + request.getServerPort() + request.getContextPath();
  }
  
}
